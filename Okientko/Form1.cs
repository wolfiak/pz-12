﻿using PZ_LAB12;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Okientko
{
    public partial class Form1 : Form
    {
       
        BindingSource bs = new BindingSource();
        public Form1()
        {
            InitializeComponent();
            kontakty = new KontaktContext();
            this.kontaktyBindingSource1.DataSource = kontakty.kontakty;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void zapiszToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            XmlSerializer xml = new XmlSerializer(typeof(List<Kontakty>));
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.DefaultExt = ".xml";
            sfd.Filter = "Pliki xml (.xml)|*.xml";
            sfd.ShowDialog();
            Stream fstream = new FileStream(sfd.FileName, FileMode.Create, FileAccess.Write, FileShare.None);
            xml.Serialize(fstream, kontakty.kontakty);
           
        //    File.WriteAllText("C:/Users/pacio/Desktop/new.xml", kontakty, Encoding.ASCII);
            Console.WriteLine("BOOOM ZAPISANE!");
            fstream.Close();
        }
        private void wczytajToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            XmlSerializer xml = new XmlSerializer(typeof(List<Kontakty>));
            OpenFileDialog ofp = new OpenFileDialog();
            ofp.Multiselect = false;
            ofp.Filter= "Pliki xml (.xml)|*.xml";
            ofp.ShowDialog();
            Stream fstream = new FileStream(ofp.FileName, FileMode.Open, FileAccess.Read, FileShare.None);
            kontakty.kontakty=(List<Kontakty>) xml.Deserialize(fstream);
            kontaktyBindingSource1.DataSource = null;
            kontaktyBindingSource1.DataSource = kontakty.kontakty;
            fstream.Close();
        }
        public void kontaktyFix(object sender, EventArgs args)
        {
            DajMiKontakty kon = (DajMiKontakty)args;
            kontaktyBindingSource1.DataSource = null;
            kontaktyBindingSource1.DataSource = kon.Kontakty;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            DirectoryInfo di = new DirectoryInfo("D:/Studia/PZ/PZ-LAB12/Okientko/bin/Debug/plugins/");
            Console.WriteLine("Cos tu sie dzieje");
            foreach (FileInfo item in di.GetFiles())
            {
                Console.WriteLine($"Wykonuje tu kreciola i laduje: {item.FullName} ");
                LoadExternalModule(item.FullName);
            }

        }
        private bool LoadExternalModule(string path)
        {
            bool found = false;
            Assembly snap = null;
            try
            {
                snap = Assembly.LoadFile(path);
                var taKlasa = snap.GetTypes().Where(k => k.IsClass && k.GetInterface("Iinterfejs") != null).ToList();
                foreach (Type t in taKlasa)
                {
                    Console.WriteLine("jestem w petli");
                    Iinterfejs inter = (Iinterfejs)snap.CreateInstance(t.FullName, true);
                    inter.Wlasciwosc = kontakty.kontakty;
                    inter.onOdwierz += new EventHandler(kontaktyFix);
                    ToolStripMenuItem item = new ToolStripMenuItem(inter.nazwa[0]);
                    plikToolStripMenuItem.DropDownItems.Add(item);
                    item.Click += new EventHandler(inter.Save);
                    ToolStripMenuItem item2 = new ToolStripMenuItem(inter.nazwa[1]);
                    plikToolStripMenuItem.DropDownItems.Add(item2);
                    item2.Click += new EventHandler(inter.Load);

                }
                return true;
            }catch(Exception e)
            {
                MessageBox.Show(e.Message);
                Console.WriteLine(e.Source);
                Console.WriteLine(e.StackTrace);
                return found;

            }
        }
        private void plikToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void menuStrip2_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
    }
}
