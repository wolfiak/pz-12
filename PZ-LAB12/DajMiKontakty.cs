﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ_LAB12
{
    public class DajMiKontakty: EventArgs
    {
        public DajMiKontakty(List<Kontakty> lista) {

            this.Kontakty = lista;
        }

        public List<Kontakty> Kontakty { get; set; }
    }
}
