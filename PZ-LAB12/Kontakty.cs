﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PZ_LAB12
{
    [Serializable]
   public class Kontakty 
    {
        public string Imie { get; set; }
        public string Naziwsko { get; set; }
        public string Telefon { get; set; }
        public string Mail { get; set; }
        public string Skype { get; set; }

    }
}
