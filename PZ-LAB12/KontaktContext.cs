﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ_LAB12
{
    public class KontaktContext
    {
        public List<Kontakty> kontakty = new List<Kontakty>() { new Kontakty() };

        public Kontakty[] getKontakty()
        {
            return kontakty.ToArray();
        }
    }
}
