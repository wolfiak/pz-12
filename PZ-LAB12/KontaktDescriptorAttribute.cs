﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ_LAB12
{
    class KontaktDescriptorAttribute: System.Attribute
    {
        private string msgData;
            public KontaktDescriptorAttribute(string msg)
        {
            msgData = msg;
        }
        public KontaktDescriptorAttribute()
        {

        }
        public string Descriptor
        {
            get { return msgData;  }
            set { msgData = value; }
        }
    }
}
